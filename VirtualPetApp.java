import java.util.Scanner;


public class VirtualPetApp {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		Owl[] parliament = new Owl[1];
		
		for(int i = 0; i < parliament.length; i++) {
				System.out.println("	Owl:" + (i + 1));
				
				System.out.println("Colour:");
				String colour = reader.nextLine();

				System.out.println("Species:");
				String species = reader.nextLine();
				
				System.out.println("Face Shape:");
				String faceShape = reader.nextLine();
				
				parliament[i] = new Owl(colour,species,faceShape);
		}
		
		int last = parliament.length - 1;
		System.out.println("This is your old colour: " + parliament[last].getColour());
		System.out.println("New Colour:");
		String colourN = reader.nextLine();
		parliament[last].setColour(colourN);
		System.out.println("This is your new colour:" + parliament[last].getColour());
		
		System.out.println("Here are the information about your owl: Your owl is a " + parliament[0].getSpecies() + " its colour is " + parliament[0].getColour() + " and its face shape is a " + parliament[0].getFaceShape());
	}
}