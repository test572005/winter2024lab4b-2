public class Owl{

	private String colour;
	private String species;
	private String faceShape;
	
	public String getColour(){
		return this.colour;
	}
	
	public String getSpecies(){
		return this.species;
	}
	
	public String getFaceShape(){
		return this.faceShape;
	}
	
	public void setColour(String colour){
		this.colour = colour;
	}
	
	public Owl(String colour, String species, String faceShape) {
		
		this.colour = colour;
		this.species = species;
		this.faceShape = faceShape;
		
	}
	
	
		// instance methods 
		
		public String fly(String colour, String species) {
			return "Wow! A " + colour + " " + species + " Owl is flying right over us!";
		}
		
		public String getType(String faceShape, String species) {
			if (faceShape == "heart") { // this only works if the faceshape is in lowercase
				return species + "owls are Barn Owls, which is rare since there only 16 species!";
			}
			else if (faceShape == "round") {
				return species + " owls are True Owls, which encompass about 230 species of owl";
			
			}
			else {
				return "Your owl is an entirely new undiscovered type!";
			}
		}

}